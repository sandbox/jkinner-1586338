<?php
// Copyright 2011, Sociodyne LLC. All rights reserved.

/**
 * @file
 * Generates the form that edits the selection criteria for a relationship.
 */

require_once drupal_get_path('module', 'user_relationships') . '/user_relationships.admin.inc';

function recommended_relationships_criteria_form($form, &$form_state, $recommended_type = NULL) {
  if ($recommended_type == NULL) {
    drupal_set_title(t('New recommendation criteria'));
    $recommended_type = (object) array(
      'criteria'    => array(),
    );
  }
  else {
    drupal_set_title(t('Edit recommendation criteria'));
    $recommended_type = recommended_relationships_criteria_load($recommended_type);
  }

  $criteria = $recommended_type->criteria;

  $form['submit'] = array(
    '#submit' => array('recommended_relationships_criteria_form_submit'),
    '#type'    => 'submit',
    '#name'   => 'update',
    '#value'  => t('Update recommendation criteria'),
    '#validate' => array('recommended_relationships_criteria_form_validate'),
    '#weight' => 30,
  );

  $form['rtid'] = array(
    '#type'   => 'hidden',
    '#value'  => arg(5),
  );

  if (module_load_include('module', 'recommended_relationships_simple')) {
    $form['test'] = array(
      '#submit' => array('recommended_relationships_test_form_submit'),
      '#type'    => 'submit',
      '#name'   => 'test',
      '#value'  => t('Test run this configuration'),
      '#validate' => array('recommended_relationships_criteria_form_validate'),
      '#weight' => 31,
    );
  }

  $form['criteria'] = array(
    '#type'    => 'fieldset',
    '#title'  => t('Criteria'),
    '#weight' => 10,
    '#group' => 'settings',
  );

  $form['threshold'] = array(
    '#type'    => 'textfield',
    '#title'  => t('Threshold'),
    '#size'    => 6,
    '#weight' => 20,
  );
  if (!empty($recommended_type->threshold)) {
    $form['threshold']['#default_value'] =
        empty($form_state['input']['threshold']) ? $recommended_type->threshold
            : $form_state['input']['threshold'];
  }

  $module_bundles = recommended_relationships_field_bundles_load();
  $module_fields = array();

  // Add 'field' => 'table.field' for sorting
  $table_header = array(
    'field_name'          => array('data' => t('Field name')),
    'weight'              => array('data' => t('Weight')),
    'comparison'          => array('data' => t('Comparison')),
  );

  foreach ($module_bundles as $module_name => $bundles) {
    foreach ($bundles as $bundle_name => $fields) {
      $options = array();
      $form['criteria'][$module_name . '_' . $bundle_name] = array(
        '#type'    => 'fieldset',
        '#title'  => check_plain($module_name . ' :: ' . $bundle_name),
        '#group' => 'settings',
      );
      $selected_rows = array();
      foreach ($fields as $field) {
        if (! isset($form['criteria'][$module_name])) {
          $display_settings = field_extra_fields_get_display($module_name, $bundle_name, 'account');
        }

        $comparison_functions = recommended_relationships_field_comparison_functions($field);

        $options[$field['id']]['#weight'] = $field['display']['default']['weight'];
        if ($comparison_functions) {
          if (!empty($criteria[$field['id']])) {
            $selected_rows[$field['id']] = 1;
          }
          $weight_value =
              empty($form_state['input'][$module_name . '_' . $bundle_name . '_weight'][$field['id']]) ?
                  (isset($criteria[$field['id']]) ? $criteria[$field['id']]->weight : 1) :
                  $form_state['input'][$module_name . '_' . $bundle_name . '_weight'][$field['id']];
          $comparison_value =
              empty($form_state['input'][$module_name . '_' . $bundle_name . '_comparison'][$field['id']]) ? 
                  (isset($criteria[$field['id']]) ? $criteria[$field['id']]->comparison : "") :
              $form_state['input'][$module_name . '_' . $bundle_name . '_comparison'][$field['id']];
          if (count(array_keys($comparison_functions)) > 1) {
            $options[$field['id']]['field_name'] = '<label>' . $field['label'] . '</label>';
            $options[$field['id']]['weight'] = array('data' => array(
                '#name'            =>
                    $module_name . '_' . $bundle_name . '_weight[' . $field['id'] . ']',
                '#type'            => 'textfield',
                '#size'            => 4,
                '#value'  => "$weight_value",
              )
            );
            $options[$field['id']]['comparison'] = array('data' => array(
                '#name'            =>
                    $module_name . '_' . $bundle_name . '_comparison[' . $field['id'] . ']',
                '#type'           => 'select',
                // TODO(jkinner): Get the label for the field type
                '#options'        => $comparison_functions,
                '#size'           => 1,
                '#value'          => $comparison_value,
              )
            );
          }
          else {
            $comparison_keys = array_keys($comparison_functions);
            $comparison_key = $comparison_keys[0];
            $options[$field['id']]['field_name'] = '<label>' . $field['label'] . '</label>';
            $options[$field['id']]['weight'] = array('data' => array(
                '#name'            =>
                    $module_name . '_' . $bundle_name . '_weight[' . $field['id'] . ']',
                '#type'            => 'textfield',
                '#size'            => 4,
                '#value'          => $weight_value,
              )
            );
            $options[$field['id']]['comparison'] = '<input type="hidden" name="' .
                    $module_name . '_' . $bundle_name . '_comparison[' . $field['id'] . ']' .
                    '" value="' . $comparison_key . '" />' . $comparison_functions[$comparison_key];
          }
        }
        else {
          // TODO(jkinner): Use drupal_set_message to display what fields aren't available.
        }
        $module_bundle_form = array(
          '#type'            => 'tableselect',
          '#header'          => $table_header,
          '#options'        => $options,
          '#group'          => 'settings',
          '#default_value'  => $selected_rows,
        );
      }
      $form['criteria'][$module_name . '_' . $bundle_name][$module_name . '_' . $bundle_name] = $module_bundle_form;
    }
  }  

  return $form;
}

function recommended_relationships_load_fields() {
  $user_bundle = field_info_bundles('user');
  $profile_bundle = field_info_bundles('profile2');
  return array_merge($user_bundle, $user_bundle);
}

/**
 * Form API validate callback for the criteria form.
 */
function recommended_relationships_criteria_form_validate($form, &$form_state) {
  $all_criteria = recommended_relationships_form_aggregate($form, $form_state);

  if (is_array($all_criteria) && $all_criteria) {
    foreach ($all_criteria as $module_name => $bundle_criteria) {
      foreach ($bundle_criteria as $bundle_name => $field_array) {
        $field_infos = field_info_instances($module_name, $bundle_name);
        foreach ($field_array as $field_id) {
          foreach ($field_infos as $field_name => $field_info) {
            if ($field_info['id'] == $field_id) {
              $field_config = field_info_instance($module_name, $field_info['field_name'], $bundle_name);
              if (empty($form_state['input'][$module_name . '_' . $bundle_name . '_weight'][$field_id])) {
                form_set_error($module_name . '_' . $bundle_name . '_weight[' . $field_id . ']',
                  t('You must specify the weight for "@field_name"', array('@field_name' => $field_config['label'])));
              };
              if (empty($form_state['input'][$module_name . '_' . $bundle_name . '_comparison'][$field_id])) {
                form_set_error($module_name . '_' . $bundle_name . '_comparison[' . $field_id . ']',
                  t('You must specify the comparison for "@field_name"', array('@field_name' => $field_config['label'])));
              };
            }
          }
        }
      }
    }
    
    // Only require a threshold if some value is set.
    if (empty($form_state['values']['threshold'])) {
      form_set_error('threshold', t('You must specify a threshold value.'));
    }
  }
}

/**
 * Loads the data from the form, determining which items were selected.
 */
function recommended_relationships_form_aggregate($form, $form_state) {
  $all_criteria = array();
  $module_bundles = recommended_relationships_field_bundles_load();
  foreach ($module_bundles as $module_name => $bundles) {
    foreach ($bundles as $bundle_name => $bundle) {
      $bundle_criteria = array_filter($form_state['values'][$module_name . '_' . $bundle_name]);
      if (is_array($bundle_criteria) && $bundle_criteria) {
        $all_criteria[$module_name][$bundle_name] = $bundle_criteria;
      } 
    }
  }

  return $all_criteria;
}

/**
 * Form API submit callback for the criteria form.
 */
function recommended_relationships_criteria_form_submit(&$form, &$form_state) {
//  $operations = module_invoke_all('recommended_relationship_operations', $form, $form_state);
  $all_criteria = recommended_relationships_form_aggregate($form, $form_state);

  // TODO(jkinner): Figure out another way to get this (it's loaded by the form)
  $user_relationship = user_relationships_type_load(arg(5));
  
  recommended_relationships_save($user_relationship, $form_state['input']['threshold']);

  // Now $all_criteria only contains the fields that we need to be concerned about.
  if (is_array($all_criteria) && $all_criteria) {
    foreach ($all_criteria as $module_name => $bundle_criteria) {
      foreach ($bundle_criteria as $bundle_name => $field_array) {
        $field_infos = field_info_instances($module_name, $bundle_name);
        foreach ($field_array as $field_id) {
          foreach ($field_infos as $field_name => $field_info) {
            if ($field_info['id'] == $field_id) {
              $weight = $form_state['input'][$module_name . '_' . $bundle_name . '_weight'][$field_id];
              $comparison_function = $form_state['input'][$module_name . '_' . $bundle_name . '_comparison'][$field_id];
              $relationship_id = arg(4);
              recommended_relationships_criteria_save($user_relationship, $field_info, $weight, $comparison_function);
            }
          }
        }
      }
    }
  }

  drupal_set_message(
    check_plain(t('Saved criteria for "@relationship_name".',
      array('@relationship_name' => $user_relationship->name))));
  drupal_set_message(
    check_plain(t('You will need to !run_cron to update all recommendations, or you can wait until cron runs on its own.'),
      array('!run_cron' => l(t('run cron'), 'admin/config/system/cron'))), 'warning');
  $form_state['redirect'] = "admin/config/people/relationships/recommended";
}

function recommended_relationships_save($user_relationship, $threshold) {
  db_delete('recommended_relationship_criteria')
      ->condition('rtid', $user_relationship->rtid)
      ->execute();
  db_delete('recommended_relationship')
      ->condition('rtid', $user_relationship->rtid)
      ->execute();
  db_insert('recommended_relationship')
      ->fields(array('rtid', 'threshold'), array($user_relationship->rtid, $threshold))
      ->execute();
}

function recommended_relationships_criteria_save($user_relationship, $field, $weight, $comparison_function) {
  db_insert('recommended_relationship_criteria')
      ->fields(array('rtid', 'fid', 'weight', 'comparison'),
          array($user_relationship->rtid, $field['id'], $weight, $comparison_function))
      ->execute();
}

/**
 * Implements hook_admin_settings()
 */
function recommended_relationships_admin_settings() {
  $form['criteria'] = array(
    '#type'    => 'fieldset',
    '#title'  => t('Criteria'),
    '#weight' => 10,
    '#group' => 'settings',
  );

  $bundles = recommended_relationships_fields_load();

  $available_fields = array();
  $form['criteria']['field'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Field name'),
    '#size'           => 4,
    '#description'    => t('Field to be used in the comparison.'),
    '#validate'       => array('recommended_relationships_setting_validation' => array(array(
      'is_numeric' => array('msg' => t('The recommendations per page setting is not an integer'))
    )))
  );
}

/**
 * Form API submit callback for deleting criteria.
 */
function recommended_relationships_criteria_form_submit_delete(&$form, &$form_state) {
#  $form_state['redirect'] = 'admin/recommended_relationships/' . $form_state['user_relationship_type']->id;
}

function recommended_relationships_admin_configured_list_page() {
  $relationship_types = user_relationships_types_load();
  $rows = array();
  $headers = array(t('Name'), t('Operations'));

  foreach ($relationship_types as $relationship) {
    $links = array(l(t('edit'), "admin/config/people/relationships/recommended/{$relationship->rtid}/edit"));
    if (module_load_include('module', 'recommended_relationships_simple')) {
      $links[] = l(t('test'), "admin/config/people/relationships/recommended/{$relationship->rtid}/test");
    }
    $rows[$relationship->rtid] = array(
      $relationship->name,
      join(' | ', $links)
    );
  }

  $page['recommended_relationships'] = array(
    '#type' => 'fieldset',
    '#title'  => t('Recommended Relationship Types'),
    '#weight' => 0,
  );

  $page['recommended_relationships']['list'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#empty' => t('No relationships available.'),
  );

  drupal_alter('recommended_relationships_types_list', $page);

  return $page;
}

/**
 * Finds all the comparison functions for a specific field across all modules.
 */
function recommended_relationships_field_comparison_functions($field) {
  static $field_comparison_functions;

  $comparison_functions = array();
  if (isset($field_comparison_functions[$field['field_name']])) {
    return $field_comparison_functions[$field['field_name']];
  }

  $field_config = field_info_field($field['field_name']);
  $comparison_functions = recommended_relationships_type_comparison_functions($field_config);

  foreach (module_list() as $module) {
    // Find a comparison hook
    // First, load the hooks for the type of the field, then merge in any field-specific hooks.
    $field_function = $module . '_field_compare_' . $field['field_name'];
    if (function_exists($field_function)) {
      $field_function_label = $field_function;
      if (function_exists($field_function . '_info')) {
        $field_function_label = call_user_func($field_function . '_info');
      }
      $comparison_functions[$field_function] = $field_function_label;
    }
  }
  $field_comparison_functions[$field['field_name']] = $comparison_functions;
  
  return $comparison_functions;
}

/**
 * Finds all the comparison functions for a field type across all modules.
 */
function recommended_relationships_type_comparison_functions($field_config) {
  static $type_comparison_functions = array();

  if (isset($type_comparison_functions[$field_config['type']])) {
    return $type_comparison_functions[$field_config['type']];
  }

  $comparison_functions = array();
  foreach (module_list() as $module) {
    // Find a comparison hook
    $field_function = $module . '_field_compare_' . $field_config['type'];
    if (function_exists($field_function)) {
      $field_function_label = $field_function;
      if (function_exists($field_function . '_info')) {
        $field_function_info = call_user_func($field_function . '_info');
        $field_function_label = $field_function_info['label'];
      }
      $comparison_functions[$field_function] = $field_function_label;
    }
  }
  $type_comparison_functions[$field_config['type']] = $comparison_functions;

  return $comparison_functions;
}

function recommended_relationships_form_map($form_state) {
  $relationship = array(
    'rtid' => $form_state['input']['rtid'],
    'threshold' => $form_state['input']['threshold'],
  );

  $module_bundles = recommended_relationships_field_bundles_load();
  $module_fields = array();

  $criteria = array();
  foreach ($module_bundles as $module_name => $bundles) {
    foreach ($bundles as $bundle_name => $fields) {
      foreach ($fields as $field) {
        if (!empty($form_state['values']["{$module_name}_{$bundle_name}"][$field['id']])) {
          $criteria[$field['id']] = (object) array(
            'weight' => $form_state['input'][$module_name . '_' . $bundle_name . '_weight'][$field['id']],
            'comparison' => $form_state['input'][$module_name . '_' . $bundle_name . '_comparison'][$field['id']],
            'fid' => $field['id'],
          );
        }
      }
    }
  }
  $relationship['criteria'] = $criteria;

  return (object) $relationship;
}

function recommended_relationships_test_form_submit($form, &$form_state) {
  $relationship_criteria = recommended_relationships_form_map($form_state); 
  $final_results = recommended_relationships_simple_recommend_relationships(
      array($relationship_criteria));
  $num_recommended = 0;
  foreach ($final_results as $final_result) {
    if ($final_result['is_recommended']) {
      $num_recommended++;
    }
  }

  drupal_set_message(
    check_plain(recommended_relationships_get_result_summary_message($final_results)));
  drupal_set_message(
    check_plain(t("If you like these results, remember to save your changes.")), 'warning');
  $form_state['redirect'] = FALSE;
}
