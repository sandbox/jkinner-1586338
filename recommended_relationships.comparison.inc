<?php
// Copyright 2011, Sociodyne LLC. All rights reserved.
/**
 * @file
 * Comparison functions for recommendend_relationships.
 */

/**
 * Implements hook_field_compare_list_boolean()
 */
function recommended_relationships_field_compare_list_boolean(
    $value_a, $value_b, $field, $settings = array()) {
  return $value_a == $value_b ? 0 : 1;
}

/**
 * Implements hook_field_compare_list_boolean_info()
 */
function recommended_relationships_field_compare_list_boolean_info() {
  return array(
    'label' => t('Compare boolean list'),
    'weight' => 1,
  );
}

/**
 * Implements hook_field_compare_number_integer()
 */
function recommended_relationships_field_compare_number_integer(
    $value_a, $value_b, $field, $settings = array()) {
  // Treat "NULL" as "zero".
  $value_a = $value_a == NULL ? 0 : $value_a;
  $value_b = $value_b == NULL ? 0 : $value_b;
  return abs($value_a - $value_b);
}

/**
 * Implements hook_field_compare_number_integer_info()
 */
function recommended_relationships_field_compare_number_integer_info() {
  return array(
    'label' => t('Compare integer numbers'),
    'weight' => 1,
  );
}

function recommended_relationships_field_compare_text(
    $value_a, $value_b, $field, $settings = array()) {
  // Compare and shift.
  $value = 0;
  $value_a_list = str_split(
    str_repeat(' ', max(strlen($value_a) - strlen($value_b), 0)) . strrev($value_a));
  $value_b_list = str_split(
    str_repeat(' ', max(strlen($value_b) - strlen($value_a), 0)) . strrev($value_b));
  
  while ($value_a_list || $value_b_list) {
    $value *= 13;
    $value += abs((int) ($value_a_list ? array_shift($value_a_list) : 0)
        - (int) ($value_b_list ? array_shift($value_b_list) : 0));
  }
  
  return $value;
}

function recommended_relationships_field_compare_text_info() {
  return array(
    'label' => t('Compare alphabetically'),
    'weight' => 1,
  );
}
