<?php
// Copyright 2011, Sociodyne LLC. All rights reserved.

/**
 * @file
 * Recommends relationships based on profile Fields. To use this module, first set up a User
 * Relationship representing the similarity you are checking. Then, select that relationship
 * in the Recommended Relationships configuration and decide which fields should be used to
 * establish the relationship.
 * 
 * A cron module must also be enabled to start recommending relationships.
 */

include_once("recommended_relationships.comparison.inc");

/**
 * Implements hook_help().
 * 
 * @param path Path of the site used to display help.
 * @param arg Array holding the current path as returned from arg()
 */
function recommended_relationships_help($path, $arg) {
  switch ($path) {
    case "admin/help#recommended_relationships":
      $output = '';
      $output .= '<h3>' . t("About") . "</h3>";
      $output .= '<p>' . t("The Recommended Relationships module allows the definition of "
        . "recommendations based on similarities between users. The similarities can be based on "
        . "the User core module or the Profile2 additional profile attributes.");
      return "<p>" . $output . "</p>";
      break;
  }
}

/**
 * Implements hook_menu() 
 */
function recommended_relationships_menu() {
  $items['admin/config/people/relationships/recommended'] = array(
    'title' => 'Recommendations',
    'description' => 'Recommends relationships for users based on profile similarity.',
    'page callback' => 'recommended_relationships_admin_configured_list_page',
    'access arguments' => array('access administration pages'),
    'file' => 'recommended_relationships.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/people/relationships/recommended/%/edit'] = array(
    'title' => 'Edit recommendation criteria',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('recommended_relationships_criteria_form', 5),
    'access arguments' => array('access administration pages'),
    'file' => 'recommended_relationships.admin.inc',
  );
  
  $items['admin/config/people/relationships/recommended/%/test'] = array(
    'title' => 'Edit recommendation criteria',
    'page callback' => 'recommended_relationships_test_cron',
    'page arguments' => array(5),
    'access arguments' => array('access administration pages'),
    'file' => 'recommended_relationships.admin.inc',
  );
  
  return $items;
}

/**
 * Implements hook_block_info()
 */
function recommended_relationships_block_info() {
  foreach (user_relationships_types_load() as $relationship_type) {
    $plural_name = $relationship_type->plural_name;
    $blocks['recommended_' . str_replace(' ', '_', $plural_name)] = array(
      'info' => t('Recommended') . ' ' . $plural_name,
      'cache' => DRUPAL_CACHE_PER_USER,
    );
  }
  return $blocks;
}

/**
 * Implements hook_block_view()
 */
function recommended_relationships_block_view($delta = '') {
  global $user;

  $recommendation_types = recommended_relationships_types_load(TRUE);

  foreach ($recommendation_types as $recommendation_type) {
    if ($delta == 'recommended_' . str_replace(' ', '_', $recommendation_type->plural_name)) {
      $block['subject'] = t('Recommended') . ' ' . $recommendation_type->plural_name;
      $item_list = array();
//      if (user_access('access_content')) {
        foreach (recommended_relationships_people($recommendation_type) as $recommended_user) {
          $other_user = $recommended_user->requester_id == $user->uid
            ? $recommended_user->requestee
            : $recommended_user->requester;
          $item_list[] = array('data' =>
            l($other_user->name, 'user/' . $other_user->uid));
        }
        $block['content'] = theme('item_list', array('items' => $item_list));
//      }
      return $block;
    }
  }
}

/**
 * Implements hook_user_relationships_type_load()
 */
function recommended_relationships_user_relationships_type_load($relationship_types_list) {
  // Relationship types have changed; make sure we also change the recommended relationships.
  $current_recommendation_types_list = recommended_relationships_types_load();
  if ($current_recommendation_types_list) {
    foreach ($relationship_types_list as $relationship_type) {
      $found = FALSE;
      foreach ($current_recommendation_types_list as $current_recommendation_type) {
        if ($current_recommendation_type->rtid == $relationship_type->rtid) {
        }
      }
    }
  }
}

function recommended_relationships_type_load($relationship_type) {
  error_log("Loading recommended type $relationship_type");
  
  return (object) array('rtid' => 1);
}

function recommended_relationships_types_load($no_cache = FALSE) {
  static $recommendation_types_list = NULL;

  if ($recommendation_types_list === NULL || $no_cache) {
    $query = db_select("recommended_relationship", "rr");
    $query->innerJoin("user_relationship_types", "type", "rr.rtid = type.rtid");
    $query->fields("rr");
    $query->fields("type");
    $results = $query->execute();
    foreach ($results as $result) {
      $recommendation_types_list[] = $result;
    }
    module_invoke_all('recommended_relationships_types_load', $recommendation_types_list);
  }

  return $recommendation_types_list;
}

/**
 * Load people with a certain relationship to the user.
 * 
 * @param unknown_type $relationship_type the relationship type to query against.
 */
function recommended_relationships_people($relationship_type) {
  global $user;
  return user_relationships_load(array(
    'user' => $user->uid,
    'rtid' => $relationship_type->rtid,
  ), array(
    // TODO(jkinner): Replace with configured value
    'limit' => '10',
    'include_user_info' => TRUE,
    'include_twoway_reverse' => TRUE,
  ), FALSE);
}

function recommended_relationships_criteria_load($relationship_type_id) {
  $query = db_select('recommended_relationship_criteria');
  $query->condition('rtid', $relationship_type_id);
  $query->fields('recommended_relationship_criteria');
  $result = $query->execute();
  $criteria = $result->fetchAllAssoc('fid');

  $criteria_objects = array();
  foreach ($criteria as $criterion) {
    $criteria_objects[$criterion->fid] = (object) $criterion;
  }

  $query = db_select('recommended_relationship');
  $query->condition('rtid', $relationship_type_id);
  $query->fields('recommended_relationship');
  $result = $query->execute()->fetch();
  $result->criteria = $criteria_objects;
  return $result;
}

function recommended_relationships_get_result_summary_message($final_results, $elapsed_seconds = NULL) {
  $num_recommended = 0;
  foreach ($final_results as $final_result) {
    if ($final_result['is_recommended']) {
      $num_recommended++;
    }
  }

  if (count($final_results) == 1) {
    $count_final_results_message = t("1 matching user");
  }
  else {
    $count_final_results_message = t("@count_final_results matching users",
        array("@count_final_results" => count($final_results)));
  }
  if ($num_recommended == 1) {
    $message = t("@num_recommended user out of @count_final_results_message was recommended.",
        array("@num_recommended" => $num_recommended,
       "@count_final_results_message" => $count_final_results_message));
  }
  else {
    $message = t("@num_recommended users out of @count_final_results_message were recommended.",
      array("@num_recommended" => $num_recommended,
        "@count_final_results_message" => $count_final_results_message));
  }
  
  if ($elapsed_seconds !== NULL) {
    if ($elapsed_seconds == 0) {
      $elapsed_seconds = "<1";
    }
    $message = "$message (${elapsed_seconds}s)";
  }

  return $message;
}

function recommended_relationships_field_bundles_load() {
  $user_bundle = field_info_instances('user');
  $profile2_bundle = field_info_instances('profile2');
  return array('user' => $user_bundle, 'profile2' => $profile2_bundle);
}

function recommended_relationships_test_cron($relationship_type) {
  $recommendation = recommended_relationships_criteria_load($relationship_type);
  // Already detected during menu set-up
  module_load_include('module', "recommended_relationships_simple");
  $final_results = recommended_relationships_simple_recommend_relationships(array($recommendation));
  drupal_set_message(
    check_plain(recommended_relationships_get_result_summary_message($final_results)));
  drupal_goto("admin/config/people/relationships/recommended");
}
